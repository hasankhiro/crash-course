const express = require('express');
const app = express();
const mongoose = require('mongoose');

const postsRoute = require('./routes/post');
const bodyParser = require('body-parser')

require('dotenv/config');




// middlewares
// ==============================================================================================
app.use('/posts', postsRoute);

app.use(bodyParser.json());

// ==============================================================================================


// Routes 
// ===============================================================================================
// how do we start listening to the server ? 
// the answer is: 
app.listen(3000); 

app.get('/', (req, res) => {
    res.send('we are on home');
});

// ===============================================================================================


// connect to the db
mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true }, () => { console.log("connected successfuly");});
